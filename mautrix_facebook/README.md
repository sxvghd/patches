# mautrix_facebook patches

## Patches so far:
- disable_unsend

  Does not bridge message deletes to Matrix, indicates that a message has been deleted instead.
